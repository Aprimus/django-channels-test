from channels.generic.websocket import AsyncWebsocketConsumer

import json

ROOM_NAME = 'test_room'
GROUP = 'test_group'


class ChatConsumer(AsyncWebsocketConsumer):
    async def connect(self):

        self.room_name = ROOM_NAME
        self.room_group_name = GROUP

        # Join the group
        await self.channel_layer.group_add(
            self.room_group_name, self.channel_name
        )

        await self.accept()

    async def disconnect(self, code):
        # Leave the group
        await self.channel_layer.group_discard(
            self.room_group_name, self.channel_name
        )

    # receive message from websocket
    async def receive(self, text_data=None, bytes_data=None):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']

        # Send message to room group
        await self.channel_layer.group_send(
            self.room_group_name,
            {
                'type': 'chat_message',
                'message': message
            }
        )

    # Receive message from room group
    async def chat_message(self, event):
        message = event['message']

        # Send message back to websocket
        await self.send(text_data=json.dumps({
            'message': message
        }))
